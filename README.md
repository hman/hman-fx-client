# HWatch client v0.8.0-SNAPSHOT

Current feature set:

* Search (title)
* Filters
    * Date, Pages, Rating, Read count
    * Tags (currently static namespace filters)
        * Multiple selection applies union of selected namespace
* Gallery state indication
   * Green triangle: `unread`
   * Red slice: `new`
   * Red ribbon: `favorite`
   * Yellow hourglass: `metadata downloading`
   * Red warning: `missing information` (i.e. thumbnail, metadata, resources)
* Persistent viewer settings


![](.wiki/HWatchFX_v0.5.0.png)


# Changelog

## v0.8.0 

* Server API change

## v0.6.0

### Featues

* REST api error handling


### Bugfixes

* Some smaller bug fixes