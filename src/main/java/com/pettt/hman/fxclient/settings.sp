package com.pettt.hman.fxclient.configuration
app=hman-fxclient
name=FXClientSettings

// -------------------------------------
//              settings
// -------------------------------------

// language
#locale                 | String    | ""

// load search or load dashboard
#loadLastSearchOnStart  | Boolean   | false

// view content as list or as gallery
#galleryView            | Boolean   | true

// open new window for metadata info view
#metadataNewWindow      | Boolean   | true

// show or hide bracket groups in titles
#showBracketsInTitles   | Boolean   | false
#showParensInTitles     | Boolean   | false
#showCurlyInTitles      | Boolean   | false

// size limit of paginated results
#itemsPerPage           | Integer   | 25

// thumbnail dimensions
#thumbnailWidth         | Integer   | 120
#thumbnailHeight        | Integer   | 180

// load thumbnail image?
#loadThumbnailImage     | Boolean   | true

// when is a gallery marked as 'new', days
#newGalleryState        | Integer   | 14

// when is a gallery marked as 'unread', times opened
#unreadGalleryState     | Integer   | 0

// filter panels separated by ','
#filterPanels           | String    | "Language, Category, Artist, Group, Parody, Character, Favorite, Tag, Marked"

// filter panel dimensions
#filterPanelHeight      | Integer   | 200

// sort by, use string-enum converter functions!
#sortBy                 | String    | "Date (newest first)"

// ------------ server -----------------

#serverProtocol         | String    | "http://"
#serverIp               | String    | "localhost"
#serverPort             | Integer   | 6776

#serverRESTWorkSearch   | String    | "work/search"



#serverRESTTasks        | String    | "task"
#serverRESTGalleryCount | String    | "gallery/count/"
#serverRESTQuery        | String    | "query/"
#serverRESTThumbnail    | String    | "thumbnail/"
#serverRESTTags         | String    | "tag/"


// -------------------------------------
//         application state
// -------------------------------------

// text typed in the search field
#currentSearch          | String    | ""
// last search text
#lastSearch             | String    | ""


// visibility of the watch node
#watchesTreeVisible     | Boolean   | false
// visibility of the filters node
#filtersVisible         | Boolean   | false

// filters selection

#filterMinPages         | String    | ""
#filterMaxPages         | String    | ""

#filterMinReadCount     | String    | ""
#filterMaxReadCount     | String    | ""

#filterMinRating        | String    | ""
#filterMaxRating        | String    | ""

#filterMinDate          | String    | ""
#filterMaxDate          | String    | ""






// -------------------------------------
//          CUSTOM CODE
// -------------------------------------

// ----- server url method ==-----------

${
    public String getServerUrl() {
        return getServerProtocol()+getServerIp()+":"+getServerPort()+"/";
    }
}$

// ----- split filter string -----------

${
    public List<String> getFilterPanelsAsList(){
        List<String> result = new ArrayList<>();

        for (String filter : getFilterPanels().split(",")) {
            result.add(filter.trim());
        }

        return result;
    }
}$

// ----- compute day difference --------

${
    public long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return Math.abs(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    }
}$


// ----- sorting enum and converter ----

${
    public enum SortBy{
          DATE_NEWEST_FIRST("Date (newest first)", "galleryGroup.works.onlineResources.created,DESC")
        , DATE_OLDEST_FIRST("Date (oldest first)", "galleryGroup.works.onlineResources.created,ASC")
        , RATING_HIGHEST_FIRST("Rating (highest first)", "rating,DESC")
        , RATING_LOWEST_FIRST("Rating (lowest first)", "rating,ASC")
        , PAGES_HIGHEST_FIRST("Pages (highest first)", "galleryGroup.works.onlineResources.pages,DESC")
        , PAGES_LOWEST_FIRST("Pages (lowest first)", "galleryGroup.works.onlineResources.pages,ASC")
        , TITLE_A_Z("Title (a..z)", "title,ASC")
        , TITLE_Z_A("Title (z..a)", "title,DESC")
        ;

        public final String displayName;
        public final String serverString;

        SortBy(String displayName, String serverString) {
            this.displayName = displayName;
            this.serverString = serverString;
        }
    }

    public SortBy getCurrentSorting(){
        switch (getSortBy()) {
            case "Date (newest first)":
                return SortBy.DATE_NEWEST_FIRST;
            case "Date (oldest first)":
                return SortBy.DATE_OLDEST_FIRST;
            case "Rating (highest first)":
                return SortBy.RATING_HIGHEST_FIRST;
            case "Rating (lowest first)":
                return SortBy.RATING_LOWEST_FIRST;
            case "Pages (highest first)":
                return SortBy.PAGES_HIGHEST_FIRST;
            case "Pages (lowest first)":
                return SortBy.PAGES_LOWEST_FIRST;
            case "Title (a..z)":
                return SortBy.TITLE_A_Z;
            case "Title (z..a)":
                return SortBy.TITLE_Z_A;
            default:
                l.error("Sorting method '{}' not found! Sort by 'Date (newest first)' instead.");
                return SortBy.DATE_NEWEST_FIRST;
        }
    }

    public void setCurrentSorting(SortBy sorting){
        setSortBy(sorting.displayName);
    }
}$

