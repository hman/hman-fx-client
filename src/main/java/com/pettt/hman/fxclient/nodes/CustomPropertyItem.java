package com.pettt.hman.fxclient.nodes;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WritableValue;
import javafx.scene.paint.Color;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.property.editor.PropertyEditor;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;

public class CustomPropertyItem implements PropertySheet.Item {

    private Property observable;

    @Inject
    FXClientSettings settings;

    private boolean editable = true;

    private String key;
    private String category, name, description;
    StringProperty p;

    public CustomPropertyItem(String name, String category, String description, Property observable, boolean editable) {
        this(name,category,description,observable);
        this.editable = editable;
    }

    public CustomPropertyItem(String name, String category, String description, Property observable) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.observable = observable;
    }

    @Override
    public boolean isEditable(){
        return editable;
    }

    @Override
    public Class<?> getType() {
        return observable.getValue().getClass();
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Object getValue() {
        return observable.getValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setValue(Object value) {
        observable.setValue(value);
    }

    @Override
    public Optional<ObservableValue<? extends Object>> getObservableValue() {
        //return Optional.empty();
        return Optional.of(observable);
    }

    @Override
    public Optional<Class<? extends PropertyEditor<?>>> getPropertyEditorClass() {
        // for an item of type number, specify the type of editor to use
//        if (Number.class.isAssignableFrom(getType())) return Optional.of(NumberSliderEditor.class);

        // ... return other editors for other types

        return Optional.empty();
    }
}