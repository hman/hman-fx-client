package com.pettt.hman.fxclient.nodes;

import com.pettt.hman.fxclient.translation.ObservableResourceFactory;
import com.pettt.hman.fxclient.views.selectlanguage.SelectlanguagePresenter;
import com.sun.prism.ResourceFactory;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.util.Pair;
import javafx.util.StringConverter;

import javax.inject.Inject;
import java.util.Locale;

import static com.pettt.hman.fxclient.translation.SupportedLanguages.*;


/** Created by piv-k on 7/16/17. */
public class LanguageBox extends ComboBox<Pair<Locale, StringProperty>>{
    private final ObservableList<Pair<Locale,StringProperty>> comboItems = FXCollections.observableArrayList();

    public LanguageBox(){}

    public void init(ObservableResourceFactory resourceFactory) {

        setCellFactory(lv -> new ListCell<Pair<Locale,StringProperty>>(){
            @Override
            protected void updateItem(Pair<Locale, StringProperty> item, boolean empty){
                super.updateItem(item,empty);
                if(item != null) textProperty().bind(item.getValue());
            }

        });

        setConverter(new StringConverter<Pair<Locale, StringProperty>>() {
            @Override
            public String toString(Pair<Locale, StringProperty> object) {
                return object.getValue().get();
            }

            @Override //not needed
            public Pair<Locale, StringProperty> fromString(String string) {return null;}
        });

        String className = SelectlanguagePresenter.class.getCanonicalName();
        
        //add all supported languages to combobox
        StringProperty ger = new SimpleStringProperty();
        ger.bind(resourceFactory.getClassStringBinding(className,
                "german").concat(" (").concat(GERMAN).concat(")"));

        StringProperty jpn = new SimpleStringProperty();
        jpn.bind(resourceFactory.getClassStringBinding(className,
                "japanese").concat(" (").concat(JAPANESE).concat(")"));

        StringProperty eng = new SimpleStringProperty();
        eng.bind(resourceFactory.getClassStringBinding(className,
                "english").concat(" (").concat(ENGLISH).concat(")"));

        StringProperty sp = new SimpleStringProperty();
        sp.bind(resourceFactory.getClassStringBinding(className,
                "spanish").concat(" (").concat(SPANISH).concat(")"));

        StringProperty fr = new SimpleStringProperty();
        fr.bind(resourceFactory.getClassStringBinding(className,
                "french").concat(" (").concat(FRENCH).concat(")"));

        StringProperty it = new SimpleStringProperty();
        it.bind(resourceFactory.getClassStringBinding(className,
                "italian").concat(" (").concat(ITALIAN).concat(")"));

        StringProperty pol = new SimpleStringProperty();
        pol.bind(resourceFactory.getClassStringBinding(className,
                "polish").concat(" (").concat(POLISH).concat(")"));

        StringProperty rus = new SimpleStringProperty();
        rus.bind(resourceFactory.getClassStringBinding(className,
                "russian").concat(" (").concat(RUSSIAN).concat(")"));

        StringProperty ch = new SimpleStringProperty();
        ch.bind(resourceFactory.getClassStringBinding(className,
                "chinese").concat(" (").concat(CHINESE).concat(")"));

        StringProperty kr = new SimpleStringProperty();
        kr.bind(resourceFactory.getClassStringBinding(className,
                "korean").concat(" (").concat(KOREAN).concat(")"));


        comboItems.add(new Pair<>(Locale.ENGLISH,eng));
        comboItems.add(new Pair<>(Locale.GERMAN,ger));
        comboItems.add(new Pair<>(SPAIN,sp));
        comboItems.add(new Pair<>(Locale.FRENCH,fr));
        comboItems.add(new Pair<>(Locale.ITALIAN,it));
        comboItems.add(new Pair<>(POLAND,pol));
        comboItems.add(new Pair<>(RUSSIA,rus));
        comboItems.add(new Pair<>(Locale.JAPANESE,jpn));
        comboItems.add(new Pair<>(Locale.CHINESE,ch));
        comboItems.add(new Pair<>(Locale.KOREAN,kr));

        setItems(comboItems);


        promptTextProperty().bind(resourceFactory.getClassStringBinding(className,"languagePrompt"));

        selectDefaultLanguage();
    }

    private void selectDefaultLanguage() {
        for (Pair<Locale, StringProperty> pair : getItems()) {
            if(pair.getKey().equals(Locale.getDefault())){
                getSelectionModel().select(pair);
                return;
            }
        }
    }
}
