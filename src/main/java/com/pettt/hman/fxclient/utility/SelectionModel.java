package com.pettt.hman.fxclient.utility;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.scene.Node;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SelectionModel {

        ObservableList<Node> selection = FXCollections.observableArrayList();

        public void add( Node node) {
            node.setStyle(
                    "-fx-border-color: #63c500;"+
                    "-fx-border-width: 2;"

            );
            selection.add( node);
        }

        public void remove( Node node) {
            node.setStyle("-fx-background-color: transparent;");
            selection.remove( node);
        }

        public void clear() {

            while( !selection.isEmpty()) {
                remove( selection.iterator().next());
            }

        }

        public boolean contains( Node node) {
            return selection.contains(node);
        }

        public void log() {
//            System.out.println( "Items in model: " + Arrays.asList( selection.toArray()));
        }

        public ObservableList<Node> getSet(){
            return selection;
        }

    }

