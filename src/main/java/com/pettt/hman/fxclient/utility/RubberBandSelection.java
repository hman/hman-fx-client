package com.pettt.hman.fxclient.utility;

import com.pettt.hman.fxcomponents.gallery.GalleryThumb;
import javafx.beans.property.ObjectProperty;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Pagination;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class RubberBandSelection {

    private static final Logger l = LoggerFactory.getLogger( "Util | Rubber Band Selection" );
        final DragContext dragContext = new DragContext();
    private final SelectionModel selectionModel;
    Rectangle rect;

        Pane group;

        public RubberBandSelection(AnchorPane group, SelectionModel model, Node eventHandling) {
            this.selectionModel = model;
            this.group = group;

            rect = new Rectangle( 0,0,0,0);
            rect.setStroke(Color.BLUE);
            rect.setStrokeWidth(1);
            rect.setStrokeLineCap(StrokeLineCap.ROUND);
            rect.setFill(Color.LIGHTBLUE.deriveColor(0, 1.2, 1, 0.6));

            eventHandling.addEventHandler(MouseEvent.MOUSE_PRESSED, onMousePressedEventHandler);
            eventHandling.addEventHandler(MouseEvent.MOUSE_DRAGGED, onMouseDraggedEventHandler);
            eventHandling.addEventHandler(MouseEvent.MOUSE_RELEASED, onMouseReleasedEventHandler);

            l.debug("Initialized rubber band selection with group '{}' and mouse registered node '{}'", group, eventHandling);
        }

        EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                //System.err.println("Pressed!");


                if(!event.isControlDown()) return;

                dragContext.mouseAnchorX = event.getX();
                dragContext.mouseAnchorY = event.getY();



                rect.setX(dragContext.mouseAnchorX);
                rect.setY(dragContext.mouseAnchorY);

                l.info("Rect starts at {}", rect.getY());



                rect.setWidth(0);
                rect.setHeight(0);

                group.getChildren().add( rect);

                event.consume();

            }
        };

        EventHandler<MouseEvent> onMouseReleasedEventHandler = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                if( !event.isShiftDown() && !event.isControlDown()) {
                    selectionModel.clear();
                }

                if(!event.isControlDown()) {
                    rect.setX(0);
                    rect.setY(0);
                    rect.setWidth(0);
                    rect.setHeight(0);

                    group.getChildren().remove( rect);
                    return;
                }


                Set<Node> nodes = group.lookupAll(".gallery-thumb");

                for (Node node : nodes) {
                    if(node instanceof GalleryThumb){
                        if( node.getBoundsInParent().intersects( rect.getBoundsInParent())) {

                            if( event.isShiftDown()) {

                                if( !selectionModel.contains( node)) selectionModel.add( node);

                            } else if( event.isControlDown()) {

                                if( selectionModel.contains( node)) {
                                    selectionModel.remove( node);
                                } else {
                                    selectionModel.add( node);
                                }
                            } else {
                                selectionModel.add( node);
                            }

                        }
                    }
                }

//                for(Node node : tp.getChildren()){
//                    if( node instanceof GalleryThumb) {
//                    }
//                }



                selectionModel.log();

                rect.setX(0);
                rect.setY(0);
                rect.setWidth(0);
                rect.setHeight(0);

                group.getChildren().remove( rect);

                event.consume();

            }
        };

        EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Bounds boundsInScene = group.localToScene(group.getLayoutBounds());
//                System.out.println("YInScene "+ boundsInScene.getMinY());
//                System.out.println("Y "+event.getY());
//                System.out.println("SceneY "+event.getSceneY());
//                System.out.println("====");



                if(!event.isControlDown()) return;

                double offsetX = event.getX() - dragContext.mouseAnchorX;
                double offsetY = event.getY() - dragContext.mouseAnchorY;

                if( offsetX > 0)
                    rect.setWidth( offsetX);
                else {
                    rect.setX(event.getX());
                    rect.setWidth(dragContext.mouseAnchorX - rect.getX());
                }

                if( offsetY > 0) {
                    rect.setHeight( offsetY);
                } else {
                    rect.setY(event.getY() );
                    rect.setHeight(dragContext.mouseAnchorY - rect.getY());
                }

                event.consume();

            }
        };

        private final class DragContext {

            public double mouseAnchorX;
            public double mouseAnchorY;


        }
    }