package com.pettt.hman.fxclient.utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Utility {
    public static String encode(String toEncode){
        try {
            return URLEncoder.encode(toEncode,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            //UTF-8 should always be available
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    public static void sleep(Integer ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new AssertionError(e);
        }
    }
}
