package com.pettt.hman.fxclient;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.views.main.MainView;
import com.pettt.hman.fxclient.views.selectlanguage.SelectlanguageView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Locale;
import java.util.prefs.Preferences;

public class HWatchFXMain extends Application {
    {
        System.setProperty("new_ribbon","Artist");
        System.setProperty("new_ribbon_ctext","whitesmoke");
        System.setProperty("new_ribbon_cfront","green");
        System.setProperty("new_ribbon_cback","darkgreen");
    }

    private static final Logger l = LoggerFactory.getLogger( "Application | Startup" );

    public static Method msg;
    @SuppressWarnings("unused") //reflection
    public static void setMessageMethod(Method m){msg = m;}
    public static String[] args;

    public Preferences prefs = Preferences.userRoot().node(FXClientSettings.NODE);

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("HWatchFX");
        primaryStage.setOnCloseRequest(e->System.exit(0));



        if(isFirstStart()){
            l.info("Detected first application start");
            SelectlanguageView view = new SelectlanguageView();
            Scene scene = new Scene(view.getView());
            primaryStage.setScene(scene);
            primaryStage.show();
        }else{
            l.debug("Language already selected");
            MainView view = new MainView();
            Scene scene = new Scene(view.getView());
//            SettingsView view = new SettingsView();
//            Scene scene = new Scene(view.getView());
            primaryStage.setScene(scene);
            primaryStage.show();
        }
    }

    public static void main(String[] args) {
        HWatchFXMain.args = args;
        try {
            // Because we need to init the JavaFX toolkit - which usually Application.launch does
            // I'm not sure if this way of launching has any effect on anything
            new JFXPanel();

            Platform.runLater(() -> {
                // Your class that extends Application
                try {
                    HWatchFXMain t = new HWatchFXMain();
                    Stage st = new Stage();
                    t.start(st);
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            });
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public boolean isFirstStart() {
        if(prefs.get(FXClientSettings.locale_node, "").equals("")){
            return true;
        } else {
            Locale.setDefault(Locale.forLanguageTag(prefs.get(FXClientSettings.locale_node,"en")));
            l.debug("Settings default language: {}", Locale.getDefault());
            return false;
        }
    }
}
