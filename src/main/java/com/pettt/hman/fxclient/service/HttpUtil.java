package com.pettt.hman.fxclient.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HttpUtil {
    /* Http client to make http requests */
    protected CloseableHttpClient httpclient;
    /* create cookie store */
    protected CookieStore store = new BasicCookieStore();

    protected ObjectMapper mapper = new ObjectMapper();

    public HttpUtil(){
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        /* set cookies */
        HttpClientBuilder builder = HttpClientBuilder.create().setDefaultCookieStore(store);
        httpclient = builder.build();
    }


    public String get(String getUrl) throws Exception{
        HttpGet p = new HttpGet(getUrl);
        HttpResponse res = httpclient.execute(p);

        BufferedReader streamReader = new BufferedReader(new InputStreamReader(res.getEntity().getContent(), "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();

        String inputStr;
        while ((inputStr = streamReader.readLine()) != null)
            responseStrBuilder.append(inputStr);

        return responseStrBuilder.toString();
    }

    public String post(String postUrl, Object entity) throws Exception {
        HttpPost httpPost = new HttpPost(postUrl);
        String value = mapper.writeValueAsString(entity);
        StringEntity jsonData = new StringEntity(value, ContentType.APPLICATION_JSON);
        httpPost.setEntity(jsonData);


        CloseableHttpResponse response = httpclient.execute(httpPost);
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();

        String inputStr;
        while ((inputStr = streamReader.readLine()) != null)
            responseStrBuilder.append(inputStr);

        return responseStrBuilder.toString();
    }

    public int put(String putUrl, Object entity) throws Exception {
        HttpPut httpPut = new HttpPut(putUrl);
        String value = mapper.writeValueAsString(entity);
        StringEntity jsonData = new StringEntity(value, ContentType.APPLICATION_JSON);
        httpPut.setEntity(jsonData);


        CloseableHttpResponse response = httpclient.execute(httpPut);
        int code = response.getStatusLine().getStatusCode();
        response.close();

        return code;
    }


    public ObjectMapper getMapper() {
        return mapper;
    }
}
