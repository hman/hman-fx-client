package com.pettt.hman.fxclient.service;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.pojo.GenericGalleryPojo;
import javafx.concurrent.Task;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;


@SuppressWarnings({"Duplicates", "unchecked"})
@Slf4j
public class GalleryApi {
    @Inject
    private FXClientSettings settings;

    @Inject HttpUtil util;

    public Task<Long> getPageSize(List<String> filters) {
        return new Task<Long>() {
            @Override
            protected Long call() throws Exception {
                //TODO
                //create request url
                StringBuilder requestUrl = new StringBuilder(settings.getServerUrl());
                requestUrl.append(settings.getServerRESTGalleryCount());
                if(filters != null && !filters.isEmpty()){
                    requestUrl.append("?").append(filters.get(0));
                    for (int i = 1; i < filters.size(); i++) {
                        requestUrl.append("&").append(filters.get(i));
                    }
                }

                log.info("{}",requestUrl.toString());

                //get url
                String responseBody = util.get(requestUrl.toString());

                //convert to object
                Long count = util.getMapper().readValue(responseBody, Long.class);
                float pages = (float) Math.ceil(Float.valueOf(count) /Float.valueOf(settings.getItemsPerPage()));
                return (long) pages;
            }
        };
    }

    public void updateGalleryGroup(GenericGalleryPojo data) throws Exception {
                log.warn("Updating not implemented");
//        int code = util.put(settings.getServerUrl()+"gallery/group/"+data.getId(),data);
//        if(code >= 400 && code < 600) {
//            throw new IllegalArgumentException("Gallery '"+data.getId()+"' update failed");
//        }
    }
}