package com.pettt.hman.fxclient.service;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.util.List;

import static com.pettt.hman.fxclient.utility.Utility.sleep;

@SuppressWarnings("Duplicates")
@Slf4j
public class MetadataApi {
    @Inject
    private FXClientSettings settings;

    @Inject
    HttpUtil util;

    //known filter contents
    private ObservableMap<String, ObservableList<String>> filterContent = FXCollections.observableHashMap();

    public MetadataApi(){
        Runnable checkStatusOfQueries = () -> {
            while(true){
                while(settings == null){
                    log.debug("Waiting for stage initialization...");
                    sleep(1000);
                }
                try {
                    updateFilters();

                    sleep(500000);
                } catch (Exception e) {
                    log.error("Could not retrieve query status! "+e.getMessage());
                    sleep(5000);
                }
            }
        };

        Thread checkStatus = new Thread(checkStatusOfQueries);
        checkStatus.setDaemon(true);

        checkStatus.start();
    }

    public void updateFilters() throws Exception {
        //log.debug("Checking filters");

        String[] categories = getAllFilterCategories();

        String serverUrl = settings.getServerUrl();

        for (String category : categories) {
            String responseBody = util.get(serverUrl+settings.getServerRESTTags()+category+"/");

            //fetch already known content
            ObservableList<String> content = filterContent.computeIfAbsent(category.toLowerCase(), k -> FXCollections.observableArrayList());

            List tagList = util.getMapper().readValue(responseBody, List.class);
            for (Object tag : tagList) {
                String t = (String) tag;
                Platform.runLater(() -> {
                    if(!content.contains(t)) content.add(t);
                });
            }
        }
    }

    //TODO fetch from server
    private String[] getAllFilterCategories() {
        return new String[]{"tag","favorite","marked","group","rating","category","artist","parody","character","language"};
    }

    public ObservableList<String> getFilterItems(String namespace) {
        return filterContent.computeIfAbsent(namespace.toLowerCase(), k -> FXCollections.observableArrayList());
    }
}
