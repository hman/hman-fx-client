package com.pettt.hman.fxclient.service;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.pojo.*;
import javafx.concurrent.Task;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

/** Created by piv-k on 09/06/17. */
@Slf4j
public class Api {
    @Inject
    private FXClientSettings settings;

    @Inject HttpUtil util;

    public Task<List<? extends GenericGalleryPojo>> pageRequest
            (String url, SearchRequest request){
        return new Task<List<? extends GenericGalleryPojo>>() {
            @Override
            protected List<? extends GenericGalleryPojo> call() throws Exception {
                log.info("Requesting {} items at page {} from {}", request.getSize(), request.getPage(), url);

                //get url
                String responseBody = util.post(url, request);

                //convert to object
                GenericGalleryPojo[] obj = null;
                try{
                    obj = util.getMapper().readValue(responseBody, WorkPojo[].class);
                } catch (Exception ignored){
                    ignored.printStackTrace();
                }

                if(obj == null) throw new IOException("Could not convert response!");

                return Arrays.asList(obj);
            }
        };
    }

    public Task<Long> getPageSize(String url, SearchRequest request) {
        return new Task<Long>() {
            @Override
            protected Long call() throws Exception {
                return 10L; //TODO page size request

//                //TODO
//                //create request url
//                StringBuilder requestUrl = new StringBuilder(settings.getServerUrl());
//                requestUrl.append(settings.getServerRESTGalleryCount());
//                if(filters != null && !filters.isEmpty()){
//                    requestUrl.append("?").append(filters.get(0));
//                    for (int i = 1; i < filters.size(); i++) {
//                        requestUrl.append("&").append(filters.get(i));
//                    }
//                }
//
//                log.info("{}",requestUrl.toString());
//
//                //get url
//                String responseBody = util.get(requestUrl.toString());
//
//                //convert to object
//                Long count = util.getMapper().readValue(responseBody, Long.class);
//                float pages = (float) Math.ceil(Float.valueOf(count) /Float.valueOf(settings.getItemsPerPage()));
//                return (long) pages;
            }
        };
    }
}
