package com.pettt.hman.fxclient.views.main.requestError;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * 2017/01/05.
 */
public class RequestErrorPresenter implements Initializable{

    private String current = "";

    private Node oldNode = null;

    private Throwable exception = null;

    @FXML
    private TextArea exceptionText;

    private StackPane rootNode;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void onClose(ActionEvent event){
        Stage stage = Stage.class.cast(Control.class.cast(event.getSource()).getScene().getWindow());


        rootNode.getChildren().clear();

        oldNode.setOpacity(1.0);
        oldNode.setDisable(false);
        stage.getScene().setRoot((Parent) oldNode);
    }

    public void setOldNode(Node oldNode) {
        this.oldNode = oldNode;
    }

    public void setException(Throwable exception) {
        this.exception = exception;

        exceptionText.setText(exception.getMessage());
    }

    public void setRootNode(StackPane rootNode) {
        this.rootNode = rootNode;
    }
}
