package com.pettt.hman.fxclient.views.browser;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * 2017/01/05.
 */
public class BrowserPresenter implements Initializable{

    @FXML
    private WebView web;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void goTo(String url) {
        //System.out.println("going to  "+url);
        WebEngine webEngine = web.getEngine();
        webEngine.load(url);
    }

    @FXML
    public void onBack(Event e){
        //System.out.println(e);
    }
}
