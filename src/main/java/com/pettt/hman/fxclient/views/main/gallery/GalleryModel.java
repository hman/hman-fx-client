package com.pettt.hman.fxclient.views.main.gallery;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.service.Api;
import com.pettt.hman.fxclient.service.GalleryApi;
import com.pettt.hman.fxclient.views.UserNotifications;
import com.pettt.hman.fxclient.views.browser.BrowserPresenter;
import com.pettt.hman.fxclient.views.browser.BrowserView;
import com.pettt.hman.fxclient.views.main.MainDAO;
import com.pettt.hman.fxclient.views.main.sidePanel.SidePanelModel;
import com.pettt.hman.fxcomponents.gallery.GalleryThumb;
import com.pettt.hman.model.Tag;
import com.pettt.hman.pojo.GenericGalleryPojo;
import com.pettt.hman.pojo.SearchRequest;
import com.pettt.hman.utility.Misc;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Integer.*;

public class GalleryModel {
    private static final Logger l = LoggerFactory.getLogger( "Application | Main Gallery" );

    @Inject
    private FXClientSettings settings;

    @Inject
    private MainDAO mainModel;

    @Inject
    private SidePanelModel sidePanelModel;

    @Inject
    private UserNotifications notifications;

    private
    AtomicLong requestId = new AtomicLong(0);

    @Inject
    private Api api;


    // PAGE CONTENT
    private Map<Integer,List<GalleryThumb>> pages = new HashMap<>();

    public void invalidatePages() {
        l.trace("Invalidated model & refreshing page size");
        pages.clear();

        long currentRequest = requestId.incrementAndGet();

        //FIXME url and request for pages
        Task<Long> size = api.getPageSize("url", createSearchRequest());

        size.setOnSucceeded(e -> {
            if(currentRequest == requestId.get()){
                setMaxPages(Math.toIntExact(size.getValue()));
            }
        });

        size.setOnFailed(e -> {
            notifications.errorServerNotReachable();
            setMaxPages(500);
        });

        new Thread(size).start();
    }

    private SearchRequest createSearchRequest() {
        SearchRequest request = new SearchRequest();

        request.setPage(currentPage.get());
        request.setSize(settings.getItemsPerPage());

        request.setSearch(settings.getCurrentSearch());

        //rating
        if(!settings.getFilterMaxRating().isEmpty()) request.setMaxRating(new BigDecimal(settings.getFilterMaxRating()));
        if(!settings.getFilterMinRating().isEmpty()) request.setMinRating(new BigDecimal(settings.getFilterMinRating()));

        //read count
        if(!settings.getFilterMinReadCount().isEmpty()) request.setMinRead(valueOf(settings.getFilterMinReadCount()));
        if(!settings.getFilterMaxReadCount().isEmpty()) request.setMaxRead(valueOf(settings.getFilterMaxReadCount()));

        //pages
        if(!settings.getFilterMinPages().isEmpty()) request.setMinPages(valueOf(settings.getFilterMinPages()));
        if(!settings.getFilterMaxPages().isEmpty()) request.setMaxPages(valueOf(settings.getFilterMaxPages()));

        //age
        if(!settings.getFilterMinDate().isEmpty()) request.setMinOldMinutes(valueOf(settings.getFilterMinDate())*24*60);
        if(!settings.getFilterMaxDate().isEmpty()) request.setMaxOldMinutes(valueOf(settings.getFilterMaxDate())*24*60);

        //tags
        List<String> tagFilter = createFilters();
        for (String tags : tagFilter) {
            request.includeTags(tags);
        }

        return request;
    }

    public List<GalleryThumb> getItems(int pageIndex){
        l.info("Get items for page {} (size {})",pageIndex, settings.getItemsPerPage());

        if(!pages.containsKey(pageIndex)){
            //start loading
            startLoading(pageIndex);
            return null;
        }

        l.trace("Fetch default {}",pages.get(pageIndex));
        return pages.getOrDefault(pageIndex, new ArrayList<>());
    }


    private void startLoading(int pageIndex){
        l.info("[] Loading page {} with filters: {}", pageIndex, filters);

        String url = settings.getServerUrl()+ settings.getServerRESTWorkSearch();

        Task<List<? extends GenericGalleryPojo>> task = api.pageRequest(url, createSearchRequest());

        task.setOnSucceeded(workerStateEvent -> {
            List<? extends GenericGalleryPojo> result = task.getValue();

            List<GalleryThumb> thumbs = generateGalleryThumbs(task.getValue());
            pages.put(pageIndex,thumbs);

            l.info("Request fetched {} galleries", thumbs.size());

            setRefreshPage(true);
        });

        task.setOnFailed(e -> {
            l.error("Loading failed: {}", task.getException().getMessage());
            connectError = true;
            setRefreshPage(true);
        });

        new Thread(task).start();
    }

    private List<String> createFilters() {
        List<String> filter = new ArrayList<>();

        for (String namespace : filters.keySet()) { //union inside filter, intersection between filters
            if(!filters.get(namespace).isEmpty()){
                String unionFilter = "";
                for (String tag : filters.get(namespace)) {
                    unionFilter += (Misc.encode(namespace.toLowerCase())+":"+Misc.encode(tag.toLowerCase())+",");
                }

                filter.add(unionFilter);
            }
        }

        return filter;
    }

    private List<GalleryThumb> generateGalleryThumbs(List<? extends GenericGalleryPojo> result) {
        List<GalleryThumb> container = new ArrayList<>();

        for (GenericGalleryPojo gallery : result) {

            GalleryThumb tb = new GalleryThumb();
            tb.setUserData(gallery);

            String title = gallery.getTitle();

            //TODO fetch online resources
            if (!gallery.getOnlineUrls().isEmpty()) {
                String finalUrl = gallery.getOnlineUrls().get(0);
                tb.setOnMouseClicked(e -> {
                    if(!e.isStillSincePress()) return;
                    browser(finalUrl);
                    tb.setUnreadTitle(false);
                    //TODO update
//                    gallery.incrementReadCount();
//                    updateGalleryGroup((GalleryGroup) tb.getUserData());
                });
            }

            tb.setOnMouseEntered(e -> {
                if (mainModel.selectionModel.getSet().isEmpty()){
                    mainModel.currentSelectedProperty().set(title);
                }
            });

            tb.getInfoButton().setOnAction(e -> {
                sidePanelModel.setMetadata(gallery);
                mainModel.setShowMetadata(tb);
            });

//            //fav button
            tb.getFavButton().setOnAction(e -> {
                //todo favorite update
//                WorkDTO data = (WorkDTO) tb.getUserData();
//                if(data.getFavorites() > 0)
//                if(data.getFavorites().contains("favorite")){
//                    data.removeFavorite("favorite");
//                }else{
//                    data.addFavorite("favorite");
//                }
//                tb.setFavTitle(data.getFavorite());
//                updateGalleryGroup(data);
            });



            tb.setOnMouseExited(e -> {
                if (mainModel.selectionModel.getSet().isEmpty()){
                    mainModel.currentSelectedProperty().set("");
                }
            });

            tb.setTitle(title);

            //new state
            Date date = gallery.getCreated();
            if (settings.getDifferenceDays(new Date(), date) < settings.getNewGalleryState()) {
                tb.newTitleProperty().set(true);
            }

            //TODO tags warning
            if(gallery.getTags().contains(new Tag("nhentai metadata queue","marked"))){
//                tb.setHourglassTooltip("Queued for metadata download from nhentai.net");
            } else {
                tb.setHourglassTooltip("Currently not!");
            }

            //unread
            if(gallery.getReadCount() == 0){
                tb.setUnreadTitle(true);
            }

            //warning tooltip
            StringBuilder warning = new StringBuilder();
            if(gallery.getTags().isEmpty()){
                warning.append("Gallery has no metadata!\n");
            }

            if(gallery.getOnlineUrls().isEmpty() && gallery.getOfflineUrl() == null) {
                warning.append("Gallery has no resources!\n");
            }

            if(gallery.getThumbnailHash().equalsIgnoreCase("-1")){
                warning.append("Gallery has no thumbnail!");
            }
            tb.setWarningTooltip(warning.toString());

            //fav
            if(gallery.getFavorites() > 0) {
                tb.setFavTitle(true);
            }

            //rating
            tb.getBar().ratingProperty().set(gallery.getRating());

            container.add(tb);

            try{
                fetchThumbnail(tb, settings.getServerUrl()+gallery.getThumbnailUrl());
            } catch (Exception e){
                l.error("Could not catch thumbnail");
            }

            //rating
            tb.getBar().ratingProperty().addListener(e -> {
                //TODO rating update
//                GalleryGroup data = ((GalleryGroup) tb.getUserData());
//                data.setRating(new BigDecimal(tb.getBar().ratingProperty().get()));
//                updateGalleryGroup(data);
            });
        }

        return container;
    }

    private void updateGalleryGroup(GenericGalleryPojo data) {
        try {
//            api.updateGalleryGroup(data);
        } catch (Exception e1) {
            notifications.errorFailedToUpdateGroup(e1.getMessage());
            l.error("Update failed!");
        }
    }


    private void fetchThumbnail(GalleryThumb tb, String thumbnail) {
        if(thumbnail.isEmpty()) return; //TODO error message

        Task<Void> fetchBackground = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                URL url;
                url = new URL(thumbnail);
                BufferedImage img = ImageIO.read(url);

                Platform.runLater(() -> tb.setImage(SwingFXUtils.toFXImage(img,null)));

                return null;
            }
        };


        new Thread(fetchBackground).start();
    }


    @SuppressWarnings("Duplicates")
    public void browser(String url) {

        Stage stage = new Stage();
        stage.setTitle("Gallery Browser");


        BrowserView browserView = new BrowserView();
        BrowserPresenter presenter = (BrowserPresenter) browserView.getPresenter();
        presenter.goTo(url);
        Platform.runLater(() -> {
            stage.setScene(new Scene(browserView.getView()));
            stage.show();
        });
    }

    // MAX PAGES
    private IntegerProperty maxPages = new SimpleIntegerProperty(1);
    public IntegerProperty maxPagesProperty() { return maxPages; }
    public int getMaxPages()                  { return maxPages.get(); }
    public void setMaxPages(int maxPages)     { this.maxPages.set(maxPages); }

    // CURRENT PAGE
    private IntegerProperty currentPage = new SimpleIntegerProperty(0);
    public IntegerProperty currentPageProperty() { return currentPage; }
    public int getCurrentPage()                  { return currentPage.get(); }
    public void setCurrentPage(int currentPage)     { this.currentPage.set(currentPage); }

    public void nextPage() {
        if(currentPage.get() < maxPages.get() - 1){
            currentPage.set(currentPage.get() + 1);
            refreshPage.set(true);
        }
    }

    public void previousPage() {
        if(currentPage.get() > 0 ){
            currentPage.set(currentPage.get() - 1);
            refreshPage.set(true);
        }
    }

    public void goToPage(int page){
        currentPage.set(page);
        refreshPage.set(true);
    }

    // REFRESH PROPERTY
    private BooleanProperty refreshPage = new SimpleBooleanProperty(false);
    public boolean isRefreshPage() { return refreshPage.get(); }
    public BooleanProperty refreshPageProperty() { return refreshPage; }
    public void setRefreshPage(boolean refreshPage) { this.refreshPage.set(refreshPage); }

    private boolean connectError = false;
    public boolean isConnectError() {
        return connectError;
    }
    public void setConnectError(boolean connectError) {
        this.connectError = connectError;
    }


    //each filter is added to the request url as
    // &filter=namespace:item,namespace2:item2 ... etc
    private ObservableMap<String, List<String>> filters = FXCollections.observableHashMap();

    public void setFilter(String namespace, ObservableList<String> selectedItems) {
        List<String> filter = filters.computeIfAbsent(namespace, k -> new ArrayList<>());
        filter.clear();
        filter.addAll(selectedItems);
        filters.put(namespace, filter);
    }
}
