package com.pettt.hman.fxclient.views.main.sidePanel;

import com.pettt.hman.pojo.GenericGalleryPojo;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class SidePanelModel {

    public GenericGalleryPojo getMetadata() {
        return metadata.get();
    }

    public ObjectProperty<GenericGalleryPojo> metadataProperty() {
        return metadata;
    }

    public void setMetadata(GenericGalleryPojo metadata) {
        this.metadata.set(metadata);
    }

    private ObjectProperty<GenericGalleryPojo> metadata = new SimpleObjectProperty<>();

}
