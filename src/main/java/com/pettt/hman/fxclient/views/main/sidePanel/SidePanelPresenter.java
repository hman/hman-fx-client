package com.pettt.hman.fxclient.views.main.sidePanel;

import com.pettt.hman.fxclient.views.browser.BrowserPresenter;
import com.pettt.hman.fxclient.views.browser.BrowserView;
import com.pettt.hman.fxclient.views.main.MainDAO;
import com.pettt.hman.fxcomponents.gallery.GalleryThumb;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.net.URL;
import java.util.*;

public class SidePanelPresenter implements Initializable {
    @Inject
    private MainDAO mainModel;

    @Inject
    private SidePanelModel model;

    @FXML
    private FlowPane artists;
    @FXML
    private FlowPane parodies;
    @FXML
    private FlowPane characters;
    @FXML
    private FlowPane groups;
    @FXML
    private FlowPane languages;
    @FXML
    private FlowPane tags;
    @FXML
    private FlowPane categories;

    @FXML
    private Node artistsLb;
    @FXML
    private Node parodiesLb;
    @FXML
    private Node charactersLb;
    @FXML
    private Node languagesLb;
    @FXML
    private Node categoriesLb;
    @FXML
    private Node tagsLb;
    @FXML
    private Node groupsLb;

    @FXML
    private GridPane infoGrid;

    @FXML
    private Label title;

    @FXML
    private ImageView cover;

    @FXML
    void onClose(){
        System.err.println("set "+mainModel.getShowMetadata());
        mainModel.setShowMetadata(null);
    }

    private Map<String, FlowPane> map;
    private Map<String, Node> mapLb;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        map = createMapping();
        mapLb = createMappingLb();
        bindVisibility();
        List<String> ignored = createIgnored();
        mainModel.showMetadataProperty().addListener(c -> {
            GalleryThumb gallery = mainModel.showMetadataProperty().get();
            if(gallery == null) return;


            map.forEach((k,v) -> v.getChildren().clear());

            cover.setImage(gallery.getImage());
            cover.setOnMouseClicked(mouseEvent -> {
//                String urll = model.getMetadata().getOnlineResources().iterator().next().getUrl();
//                browser(urll);
            });

            title.setText(gallery.getTitle());

            for (String tag : model.getMetadata().getTags()) {
                Label lb = new Label(tag.split(":")[1]);
                lb.getStyleClass().add("tag");
                map.getOrDefault(tag.split(":")[0], tags).getChildren().add(lb);
            }


        });
    }


    private void bindVisibility() {
        map.forEach((k,v) -> {
            v.visibleProperty().bind(Bindings.isNotEmpty(v.getChildren()));
            v.managedProperty().bind(Bindings.isNotEmpty(v.getChildren()));

            mapLb.get(k).visibleProperty().bind(Bindings.isNotEmpty(v.getChildren()));
            mapLb.get(k).managedProperty().bind(Bindings.isNotEmpty(v.getChildren()));
        });

    }

    //temporary solution to ignore unwanted tags
    private List<String> createIgnored() {

        return Arrays.asList("title", "url", "t_alt", "thumbnail");
    }

    //temporary solution for mapping
    private Map<String, FlowPane> createMapping() {
        Map<String, FlowPane> map = new HashMap<>();
        map.put("artist",artists);
        map.put("parody",parodies);
        map.put("group",groups);
        map.put("language",languages);
        map.put("character",characters);
        map.put("category",categories);
        map.put("tag",tags);

        return map;
    }

    private Map<String, Node> createMappingLb() {
        Map<String, Node> map = new HashMap<>();
        map.put("artist",artistsLb);
        map.put("parody",parodiesLb);
        map.put("group",groupsLb);
        map.put("language",languagesLb);
        map.put("character",charactersLb);
        map.put("category",categoriesLb);
        map.put("tag",tagsLb);

        return map;
    }

    public void browser(String url) {

        Stage stage = new Stage();
        stage.setTitle("Gallery Browser");


        BrowserView browserView = new BrowserView();
        BrowserPresenter presenter = (BrowserPresenter) browserView.getPresenter();
        presenter.goTo(url);
        Platform.runLater(() -> {
            stage.setScene(new Scene(browserView.getView()));
            stage.show();
        });
    }
}
