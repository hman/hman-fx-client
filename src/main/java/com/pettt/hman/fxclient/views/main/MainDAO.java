package com.pettt.hman.fxclient.views.main;

import com.pettt.hman.fxclient.service.Api;
import com.pettt.hman.fxclient.utility.SelectionModel;
import com.pettt.hman.fxcomponents.gallery.GalleryThumb;
import javafx.beans.property.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.*;

/** Created by piv-k on 7/10/17. */
public class MainDAO {
    private static final Logger l = LoggerFactory.getLogger( "Application | Main Model" );

    @Inject
    private Api api;

    public MainDAO(){
        startWatchingFilters();
    }

    List<GalleryThumb> currentGalleries = new ArrayList<>();

    public void clear() {
        currentGalleries.clear();
    }

    public void add(GalleryThumb tb) {
        currentGalleries.add(tb);
    }


    public List<GalleryThumb> getElements(Integer index, int itemCount) {
        List<GalleryThumb> thumbs = new ArrayList<>();

        int startIndex = index*itemCount;

        for (int i = startIndex; i < startIndex+itemCount; i++) {
            try{
                thumbs.add(currentGalleries.get(i));
            }catch (IndexOutOfBoundsException ignored){}
        }

        return thumbs;
    }

    public List<GalleryThumb> getAllElements() {
        return currentGalleries;
    }



    // SELECTION MODEL
    public SelectionModel selectionModel = new SelectionModel();

    public int getNumberOfElements() {
        return getAllElements().size();
    }


    private StringProperty currentSelected = new SimpleStringProperty("");

    public StringProperty currentSelectedProperty() {
        return  currentSelected;
    }

    private BooleanProperty selectedSide = new SimpleBooleanProperty(true);
    public boolean isSelectedSide() { return selectedSide.get(); }
    public BooleanProperty selectedSideProperty() { return selectedSide; }
    public void setSelectedSide(boolean selectedSide) { this.selectedSide.set(selectedSide); }



    //showing metadata
    private ObjectProperty<GalleryThumb> showMetadata = new SimpleObjectProperty<>();

    public GalleryThumb getShowMetadata() { return showMetadata.get(); }
    public ObjectProperty<GalleryThumb> showMetadataProperty() { return showMetadata; }
    public void setShowMetadata(GalleryThumb showMetadata) { this.showMetadata.set(showMetadata); }

    private void startWatchingFilters(){

    }
}

