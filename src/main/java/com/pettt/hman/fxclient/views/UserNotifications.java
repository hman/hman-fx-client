package com.pettt.hman.fxclient.views;

import com.pettt.hman.fxclient.nodes.Notifications;
import javafx.util.Duration;

public class UserNotifications {
    public void errorServerNotReachable(){
        Notifications.create().darkStyle().hideAfter(Duration.INDEFINITE)
                .title("Server unreachable")
                .text("Server can not be reached. Check settings and/or connectivity.").showWarning();
    }

    public void errorFailedToUpdateGroup(String message){
        Notifications.create().darkStyle().hideAfter(Duration.INDEFINITE)
                .title("Update failed")
                .text("Could not update gallery group:\n"+message).showWarning();
    }
}
