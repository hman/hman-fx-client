package com.pettt.hman.fxclient.views.main.gallery;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.service.Api;
import com.pettt.hman.fxclient.utility.RubberBandSelection;
import com.pettt.hman.fxclient.views.main.MainDAO;
import com.pettt.hman.fxclient.views.main.MainPresenter;
import com.pettt.hman.fxcomponents.gallery.GalleryThumb;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.net.URL;
import java.util.*;

/** Created by piv-k on 7/16/17. */
public class GalleryPresenter implements Initializable {
    private static final Logger l = LoggerFactory.getLogger( "Application | Pagination" );

    private static final double CELL_H_GAP = 5;
    private static final double CELL_V_GAP = 5;
    private static final double CELL_HEIGHT = 230;
    private static final double CELL_WIDTH = 128;
    private int itemCount = 25; // Count of items that fit on a page

    @Inject
    private MainDAO dao;

    @Inject
    private Api api;

    @Inject
    private FXClientSettings settings;

    @Inject
    private GalleryModel model;

    @Inject MainDAO mainModel;

    private MainPresenter mainPresenter;

    @FXML
    private AnchorPane scrollRoot;

    @FXML
    private AnchorPane scrollContent;

    @FXML
    private ScrollPane scroll;


    @Override
    public void initialize(URL url, ResourceBundle resources) {
        //showPage(0);

        model.refreshPageProperty().addListener(c ->{
            if(model.isConnectError()){
                model.setConnectError(false);
                model.setRefreshPage(false);

                l.debug("Error page");
                scrollContent.getChildren().clear();

                StackPane sp = new StackPane();
                sp.prefWidthProperty().bind(scrollRoot.prefWidthProperty().subtract(2));
                sp.prefHeightProperty().bind(scrollRoot.prefHeightProperty().subtract(2));
                Label lb = new Label("Connection Error");
                sp.getChildren().add(lb);

                scrollContent.getChildren().add(sp);
            }

            if(model.isRefreshPage()){
                l.info("Refreshing page {}", model.currentPageProperty().get());
                model.setRefreshPage(false);
                showPage(model.currentPageProperty().get());
            }
        });

        scroll.addEventHandler(MouseEvent.DRAG_DETECTED, e -> {
            if(e.isControlDown()) {
                scroll.setPannable(false);
            }else{
                scroll.setPannable(true);
                e.consume();
            }
        });

        scrollContent.prefWidthProperty().bind(scrollRoot.prefWidthProperty().subtract(20));

        l.info("Initialized gallery pagination");
    }

    public void showPage(int i) {
        List<GalleryThumb> list = model.getItems(i);
        if(list == null){
            l.info("Loading page...");

            StackPane sp = new StackPane();
            sp.prefWidthProperty().bind(scrollRoot.prefWidthProperty().subtract(2));
            sp.prefHeightProperty().bind(scrollRoot.prefHeightProperty().subtract(2));

            ProgressIndicator pi = new ProgressIndicator(-1);
            pi.setMaxSize(32,32);
            sp.getChildren().add(pi);

            scrollContent.getChildren().clear();
            scrollContent.getChildren().add(sp);
        } else{
            l.info("Creating gallery view!");

            FlowPane p = new FlowPane();
            p.prefWidthProperty().bind(scrollRoot.prefWidthProperty().subtract(20));
            p.maxWidthProperty().bind(scrollRoot.prefWidthProperty().subtract(20));
            p.setPadding(new Insets(5,0,20,0));
            p.setHgap(5);
            p.setVgap(5);
            p.setAlignment(Pos.CENTER);
            p.getChildren().addAll(list);

            scrollContent.getChildren().clear();
            scrollContent.getChildren().add(p);
            new RubberBandSelection(scrollContent,mainModel.selectionModel,p);

        }
    }

    private Node pageFactory( int pageIndex ) {

        return new Label("not implemented");
    }

    private void refreshItemsPerPage() {
//        l.trace("Width: {}  |  Height: {}",dragContent.getPrefWidth(), dragContent.getPrefHeight());
//        int itemCountHorizontal;
//        int itemCountVertical;
//        int itemCount;
//
//
////        itemCountHorizontal += ( currentPage.get().getWidth() - CELL_WIDTH - 1 ) / ( CELL_WIDTH + CELL_H_GAP );
//        itemCountHorizontal = (int) Math.ceil((dragContent.getPrefWidth() / CELL_WIDTH));
////        System.out.println("itemCount: "+itemCountHorizontal);
////        System.out.println("HItems * CellWidth + HGap*HItems - HGap = ?");
////        double result = (itemCountHorizontal * CELL_WIDTH + CELL_H_GAP * itemCountHorizontal - CELL_H_GAP);
////        System.out.println(itemCountHorizontal + "*" + CELL_WIDTH + " + " +CELL_H_GAP+ " * " + itemCountHorizontal+
////                " - " + CELL_H_GAP + "=" + result);
//        if(itemCountHorizontal * CELL_WIDTH + CELL_H_GAP*itemCountHorizontal - CELL_H_GAP > dragContent.getPrefWidth()){
//            itemCountHorizontal -= 1;
//        }
//
////        System.out.println("After calc: "+itemCountHorizontal);
//
//        itemCountVertical = (int) Math.ceil((dragContent.getPrefHeight() / CELL_HEIGHT));
//        if(itemCountVertical * CELL_HEIGHT + CELL_V_GAP*itemCountVertical  > dragContent.getPrefHeight()){
//            itemCountVertical -= 1;
//        }
//
////        System.out.println(itemCountVertical);
//
//        itemCount = itemCountHorizontal * itemCountVertical;
//        itemCount = ( itemCount == 0 ) ? 1 : itemCount;
//
////        System.out.println(currentPage.get().getWidth()/ CELL_WIDTH);
////        System.out.println(itemCountHorizontal);
////        System.out.println("////////////////");
//
//        settings.setItemsPerPage(itemCount);
//        l.info("Recalculated {} items per page" , settings.getItemsPerPage());
//
//        Thread t;
//        t = new Thread(new Task<Void>() {
//            @Override
//            protected Void call() throws Exception {
//                Thread.sleep(1000);
//                if(currentRefresh != Thread.currentThread().getId()){
//                    l.trace("Old refresh, discarding");
//                } else {
//                    l.trace("Refreshing");
//                }
//
//                return null;
//            }
//        });
//        t.start();
//        currentRefresh = t.getId();
//        mainPresenter.refreshMaxPageSize();
    }



    public void init(MainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
        AnchorPane root = mainPresenter.getContentRoot();

        scrollRoot.prefWidthProperty().bind(root.widthProperty());
        scrollRoot.prefHeightProperty().bind(root.heightProperty());

        //new RubberBandSelection(dragContent,dao.selectionModel, pagination);
    }

}
