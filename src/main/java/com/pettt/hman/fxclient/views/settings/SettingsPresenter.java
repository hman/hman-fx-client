package com.pettt.hman.fxclient.views.settings;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.nodes.CustomPropertyItem;
import com.pettt.hman.fxclient.views.selectlanguage.SelectlanguagePresenter;
import com.pettt.hman.fxclient.views.selectlanguage.SelectlanguageView;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.controlsfx.control.PropertySheet;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

/** Created by piv-k on 7/11/17. */
public class SettingsPresenter implements Initializable{
    @FXML
    private TreeView settingsTree;

    @FXML
    private StackPane rightSide;

    @Inject
    private FXClientSettings settings;

    private PropertySheet sheet = new PropertySheet();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        fillSettings();



        rightSide.getChildren().add(sheet);

    }

    private void fillSettings() {
        ObservableList<PropertySheet.Item> items = sheet.getItems();
        String category;
        {//gallery settings
            category = "Gallery";

            // load last search
            items.add(new CustomPropertyItem(
                    "Load last search on start",
                    category,
                    "If true, the last search is loaded. Otherwise, the normal dashboard is loaded",
                    settings.loadLastSearchOnStartProperty())
            );

            //items per page
            items.add(new CustomPropertyItem(
                    "Items per page",
                    category,
                    "Limits the number of galleries to be displayed",
                    settings.itemsPerPageProperty())
            );

            //thumbnail title brackets
            items.add(new CustomPropertyItem(
                    "Hide {} in titles",
                    category,
                    "Hides {} groups in gallery thumbnail titles. Enabled by default to shorten the full title.",
                    settings.showCurlyInTitlesProperty())
            );

            items.add(new CustomPropertyItem(
                    "Hide [] in titles",
                    category,
                    "Hides [] groups in gallery thumbnail titles. Enabled by default to shorten the full title.",
                    settings.showBracketsInTitlesProperty())
            );

            items.add(new CustomPropertyItem(
                    "Hide () in titles",
                    category,
                    "Hides () groups in gallery thumbnail titles. Enabled by default to shorten the full title.",
                    settings.showParensInTitlesProperty())
            );


        }

        {//thumbnail state
            category = "Gallery Thumbnail";

            //load thumbnail
            items.add(new CustomPropertyItem(
                    "Load Thumbnail Image",
                    category,
                    "If disabled, the gallery image is not loaded. This could save bandwith if needed.",
                    settings.loadThumbnailImageProperty())
            );

            //gallery thumbnail dimensions
            //TODO issue
            items.add(new CustomPropertyItem(
                    "Thumbnail Width",
                    category,
                    "Width of a thumbnail in the galley view. Currently fixed.",
                    settings.thumbnailWidthProperty(), false)
            );
            items.add(new CustomPropertyItem(
                    "Thumbnail Height",
                    category,
                    "Height of a thumbnail in the galley view. Currently fixed.",
                    settings.thumbnailHeightProperty(), false)
            );
        }

        {//server settings
            category = "Server";

            //server address
            items.add(new CustomPropertyItem(
                    "Server IP",
                    category,
                    "Address of the REST Api. 'localhost' can be used if the server is hosted locally.",
                    settings.serverIpProperty())
            );

            items.add(new CustomPropertyItem(
                    "Server Port",
                    category,
                    "Port of the REST Api. '8081' is the default port.",
                    settings.serverPortProperty())
            );
        }
    }

    @FXML
    void onGallery(ActionEvent event) {

    }

    @FXML
    void onHistory(ActionEvent event) {

    }

    @FXML
    void onHotkeys(ActionEvent event) {

    }

    @FXML
    void onLanguage(ActionEvent event) {
        SelectlanguageView view = new SelectlanguageView();
        ((SelectlanguagePresenter) view.getPresenter()).setSettingsView();
        rightSide.getChildren().clear();

        VBox box = new VBox(view.getView());
        box.setFillWidth(true);
        box.setAlignment(Pos.TOP_CENTER);
        rightSide.getChildren().add(box);
    }

    @FXML
    void onServer(ActionEvent event) {

    }

    @FXML
    void onUpdates(ActionEvent event) {

    }
}
