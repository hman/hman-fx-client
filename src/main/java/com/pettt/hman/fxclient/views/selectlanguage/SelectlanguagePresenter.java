package com.pettt.hman.fxclient.views.selectlanguage;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.nodes.LanguageBox;
import com.pettt.hman.fxclient.translation.ObservableResourceFactory;
import com.pettt.hman.fxclient.views.selectsetup.SelectsetupView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javax.inject.Inject;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;


/**
 * 2017/01/04.
 */
public class SelectlanguagePresenter implements Initializable{

    @Inject
    private ObservableResourceFactory resourceFactory;

    @Inject
    private FXClientSettings settings;

    @FXML
    private AnchorPane root;

    @FXML
    private Label selectLb;

    @FXML
    private Button confirmBtn;

    @FXML
    private Button exitBtn;

    @FXML
    private LanguageBox languageSelect;

    private boolean isSettings = false;

    private static final String className = SelectlanguagePresenter.class.getCanonicalName();

    @FXML
    void onConfirm(ActionEvent event) {

        Stage stage = Stage.class.cast(Control.class.cast(event.getSource()).getScene().getWindow());
        SelectsetupView view = new SelectsetupView();
        stage.getScene().setRoot(view.getView());
    }


    @FXML
    void onExit() {
        System.exit(0);
    }

    /**
     * Changes default locale and reloads view
     */
    @FXML
    void onLanguageSelect() {
        Locale.setDefault(languageSelect.getSelectionModel().getSelectedItem().getKey());
        settings.setLocale(Locale.getDefault().toLanguageTag());
        resourceFactory.updateClassResources();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceFactory.updateClassResources();

        languageSelect.init(resourceFactory);

        selectLb.textProperty().bind(resourceFactory.getClassStringBinding(className,"selectLanguage"));
        confirmBtn.textProperty().bind(resourceFactory.getClassStringBinding(className,"confirmLanguage"));
        exitBtn.textProperty().bind(resourceFactory.getClassStringBinding(className,"exitSetup"));
    }

    public void setSettingsView(){
        confirmBtn.setVisible(false);
        exitBtn.setVisible(false);
        isSettings = true;
        
        root.setPrefWidth(Region.USE_COMPUTED_SIZE);
    }
}
