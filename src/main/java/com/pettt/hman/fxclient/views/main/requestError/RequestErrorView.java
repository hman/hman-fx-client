package com.pettt.hman.fxclient.views.main.requestError;

import com.airhacks.afterburner.views.FXMLView;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

/**
 * 2017/01/04.
 */
public class RequestErrorView extends FXMLView {
    public RequestErrorView(StackPane rootNode, Node oldNode, Throwable exception) {
        ((RequestErrorPresenter) getPresenter()).setOldNode(oldNode);
        ((RequestErrorPresenter) getPresenter()).setException(exception);
        ((RequestErrorPresenter) getPresenter()).setRootNode(rootNode);
    }
}
