package com.pettt.hman.fxclient.views.selectsetup;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.views.main.MainView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Control;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * 2017/01/05.
 */
public class SelectsetupPresenter implements Initializable {

    @Inject
    private FXClientSettings settings;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    void onDistributed(ActionEvent event) {
        //TODO implement
    }

    @FXML
    void onExit() {
        System.exit(0);
    }

    @FXML
    void onSingleComputer(ActionEvent event) {
        //TODO implement
    }

    @FXML
    void onStartTour(ActionEvent event) {
        Stage stage = Stage.class.cast(Control.class.cast(event.getSource()).getScene().getWindow());
        MainView view = new MainView();
        stage.getScene().setRoot(view.getView());
    }
}
