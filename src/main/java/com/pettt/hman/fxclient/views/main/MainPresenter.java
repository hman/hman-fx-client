package com.pettt.hman.fxclient.views.main;

import com.pettt.hman.fxclient.configuration.FXClientSettings;
import com.pettt.hman.fxclient.nodes.Notifications;
import com.pettt.hman.fxclient.service.Api;
import com.pettt.hman.fxclient.service.MetadataApi;
import com.pettt.hman.fxclient.views.main.gallery.GalleryModel;
import com.pettt.hman.fxclient.views.main.gallery.GalleryPresenter;
import com.pettt.hman.fxclient.views.main.gallery.GalleryView;
import com.pettt.hman.fxclient.views.main.list.ListTablePresenter;
import com.pettt.hman.fxclient.views.main.list.ListTableView;
import com.pettt.hman.fxclient.views.main.requestError.RequestErrorView;
import com.pettt.hman.fxclient.views.main.sidePanel.SidePanelView;
import com.pettt.hman.fxclient.views.settings.SettingsView;
import com.pettt.hman.fxcomponents.flowgrid.FlowGridPane;
import com.pettt.hman.fxcomponents.gallery.GalleryThumb;
import com.pettt.hman.fxcomponents.namespacefilter.FilterBox;
import com.pettt.hman.fxcomponents.namespacefilter.NamespaceFilter;
import com.pettt.hman.fxcomponents.namespacefilter.RangeFilterBox;
import com.pettt.hman.fxcomponents.progress.ProgressMenu;
import com.pettt.hman.fxcomponents.search.AdvancedSearchField;
import com.pettt.hman.fxcomponents.search.AutoCompleteTextField;
import com.pettt.hman.fxcomponents.treeview.TreeViewWithItems;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.net.URL;
import java.util.Collections;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.pettt.hman.fxclient.configuration.FXClientSettings.SortBy.*;
import static com.pettt.hman.fxclient.views.main.ApiType.None;

/**
 * 2017/01/05.
 */
@SuppressWarnings("Duplicates")
public class MainPresenter implements Initializable{
    private static final Logger l = LoggerFactory.getLogger( "Application | Main" );

    @Inject
    private FXClientSettings settings;

    @Inject
    private MetadataApi metadataApi;
    @Inject
    private GalleryModel galleryModel;

    @FXML
    private Label currentQuery;

    @FXML
    private Label currentSelected;

    @FXML
    private ProgressMenu progress;

    @FXML
    private AdvancedSearchField search;

    @FXML
    private BorderPane rootPane;

    @FXML
    private StackPane contentStack;

    @FXML
    private AnchorPane contentAnchor;

    @Inject
    private MainDAO dao;

    private GalleryView galleryView;
    private GalleryPresenter galleryPresenter;
    private ListTableView tableView;
    private ListTablePresenter listPresenter;
    private Parent sidePanel;

    @FXML
    private Button galleryIndicator;

    @FXML
    private Button listIndicator;

    @FXML
    private SplitMenuButton sortButton;

    @FXML
    private HBox rightSelectedBox;

    @FXML
    private HBox rangeFilterBox;

    @FXML
    private TextField currentPage;


    @FXML
    private Node filterPane;

    @FXML
    private FlowGridPane filterPaneFilters;

    @FXML
    private StackPane pageIndicator;

    @FXML
    private TreeViewWithItems watchesTree;

    @FXML
    private Node leftPanel;

    @FXML
    private Label maxPagesLabel;

    //NOT IN FXML
    private RangeFilterBox dateFilter;
    private RangeFilterBox pagesFilter;
    private RangeFilterBox ratingFilter;
    private RangeFilterBox readFilter;

//    private void handleMouseClicked(MouseEvent event) {
//        Node node = event.getPickResult().getIntersectedNode();
//        // Accept clicks only on node cells, and not on empty spaces of the TreeView
//        if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
//            TreeItem<Pair<String, ApiType>> n = watchesTree.getSelectionModel().getSelectedItem();
//            int index = watchesTree.getSelectionModel().getSelectedIndex();
//            l.warn("Clicked: {}@{}", n, index);
//
//            switch (n.getValue().getValue()) {
//                case QueryId:
//                    break;
//                case None:
//                    break;
//            }
//        }
//    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initViewsNotInFXML();
        bindVisibility();
        bindToPersistentSettings();
        bindToApiContent();

        populateWatchesTree();
//        watchesTree.setOnMouseClicked(this::handleMouseClicked);

//        watchesTree.setCellFactory(new Callback<TreeView<Pair<String, ApiType>>, TreeCell<Pair<String, ApiType>>>() {
//            @Override
//            public TreeCell<Pair<String, ApiType>> call(TreeView<Pair<String, ApiType>> pairTreeView) {
//                return new TreeCell<Pair<String,ApiType>>(){
//                    @Override
//                    protected void updateItem(Pair<String, ApiType> item, boolean empty) {
//                        super.updateItem(item, empty);
//
//                        if (empty) {
//                            setText(null);
//                            setGraphic(null);
//                        } else {
//                            setText(item.getKey());
//                        }
//                    }
//                };
//            }
//        });



        currentPage.prefColumnCountProperty().bind(currentPage.textProperty().length());
        galleryModel.currentPageProperty().addListener(c -> currentPage.setText(String.valueOf(galleryModel.getCurrentPage() + 1)));
        maxPagesLabel.textProperty().bind(galleryModel.maxPagesProperty().asString());



        // bind request counters
        //TODO
//        progress.activeRequestsProperty().bind(api.activeRequests);
//        progress.queuedRequestsProperty().bind(api.enqueuedRequests);
//        progress.errorRequestsProperty().bind(api.errorRequests);

        // bind query term and current search
        currentQuery.textProperty().bind(settings.lastSearchProperty());
        AutoCompleteTextField search = this.search.getTextField();
        search.setOnAction(this::onSearch);
        search.setText(settings.getCurrentSearch());
        search.textProperty().addListener(c -> settings.currentSearchProperty().set(search.getText()));

        // selected items text binding and dimension binding
        rightSelectedBox.maxWidthProperty().bind(contentAnchor.widthProperty().divide(2.7));

        rightSelectedBox.setOnMouseEntered(e -> {

            if(dao.isSelectedSide()){
                AnchorPane.setRightAnchor(rightSelectedBox,0.0);
                AnchorPane.setLeftAnchor(rightSelectedBox,null);
            }else{
                AnchorPane.setLeftAnchor(rightSelectedBox,0.0);
                AnchorPane.setRightAnchor(rightSelectedBox,null);
            }

            dao.setSelectedSide(!dao.isSelectedSide());
        });


        currentSelected.textProperty().bind(dao.currentSelectedProperty());
        dao.selectionModel.getSet().addListener((ListChangeListener<Node>) change -> {
            dao.currentSelectedProperty().set("");
            String label = "Selected:";
            for (Node node : dao.selectionModel.getSet()) {
                GalleryThumb thumb = ((GalleryThumb) node);
                label += "\n"+thumb.getTitle();
            }
            dao.currentSelectedProperty().set(label);
        });

        //initialize both gallery and table view
        galleryView = new GalleryView();
        Parent root = galleryView.getView();
        AnchorPane.setTopAnchor(root,0.);
        AnchorPane.setBottomAnchor(root,0.);
        AnchorPane.setRightAnchor(root,0.);
        AnchorPane.setLeftAnchor(root,0.);
        galleryPresenter = (GalleryPresenter) galleryView.getPresenter();
        galleryPresenter.init(this);

        tableView = new ListTableView();
        Parent rootList = tableView.getView();
        AnchorPane.setTopAnchor(rootList,0.);
        AnchorPane.setBottomAnchor(rootList,0.);
        AnchorPane.setRightAnchor(rootList,0.);
        AnchorPane.setLeftAnchor(rootList,0.);
        listPresenter = (ListTablePresenter) tableView.getPresenter();

        if (settings.getGalleryView()) {
            contentAnchor.getChildren().add(0,root);
            setGalleryView();
        }else{
            contentAnchor.getChildren().add(0,rootList);
            setListView();
        }


        if (!settings.getLoadLastSearchOnStart()) {
            search.setText(settings.getLastSearch());
            search.fireEvent(new ActionEvent());
        }
    }

    private void initViewsNotInFXML() {
        {//range filter boxes
            dateFilter = new RangeFilterBox("Date", "days old");
            pagesFilter = new RangeFilterBox("Pages", "pages");
            ratingFilter = new RangeFilterBox("Rating", "rating");
            readFilter = new RangeFilterBox("Read Count", "times read");




            rangeFilterBox.getChildren().addAll(dateFilter, pagesFilter, ratingFilter, readFilter);
        }
        {//side panel
            sidePanel = new SidePanelView().getView();
            rootPane.setRight(sidePanel);

            sidePanel.visibleProperty().bind(dao.showMetadataProperty().isNotNull());
            sidePanel.managedProperty().bind(dao.showMetadataProperty().isNotNull());
        }
    }


    private void bindToApiContent() {
        {//filters
            for (String filter : settings.getFilterPanelsAsList()) {

                ObservableList<String> items = FXCollections.observableArrayList();
                FilterBox filterNode = new FilterBox(filter,filter);
                filterNode.prefHeightProperty().bind(settings.filterPanelHeightProperty());

                metadataApi.getFilterItems(filter).addListener((ListChangeListener<? super String>) c -> {
                    while(c.next()){
                        for (String removed : c.getRemoved()) {
                            filterNode.getData().remove(removed);
                        }
                        for (String added : c.getAddedSubList()) {
                            filterNode.getData().add(added);
                        }
                    }
                });

                filterNode.getList().getSelectionModel().getSelectedIndices().addListener((ListChangeListener<? super Integer>) c -> {
                    galleryModel.setFilter(filter,filterNode.getList().getSelectionModel().getSelectedItems());
                });



                filterPaneFilters.getChildren().add(filterNode);
            }
        }
    }

    private void bindToPersistentSettings() {
        {//filters
            {//min max date
                //load from persistence
                dateFilter.setMinFilter(settings.getFilterMinDate());
                dateFilter.setMaxFilter(settings.getFilterMaxDate());

                //add listener
                dateFilter.minFilterProperty().addListener(c -> settings.setFilterMinDate(dateFilter.getMinFilter()));
                dateFilter.maxFilterProperty().addListener(c -> settings.setFilterMaxDate(dateFilter.getMaxFilter()));
            }

            {//min max pages
                //load from persistence
                pagesFilter.setMinFilter(settings.getFilterMinPages());
                pagesFilter.setMaxFilter(settings.getFilterMaxPages());

                //add listener
                pagesFilter.minFilterProperty().addListener(c -> settings.setFilterMinPages(pagesFilter.getMinFilter()));
                pagesFilter.maxFilterProperty().addListener(c -> settings.setFilterMaxPages(pagesFilter.getMaxFilter()));
            }

            {//min max rating
                //load from persistence
                ratingFilter.setMinFilter(settings.getFilterMinRating());
                ratingFilter.setMaxFilter(settings.getFilterMaxRating());

                //add listener
                ratingFilter.minFilterProperty().addListener(c -> settings.setFilterMinRating(ratingFilter.getMinFilter()));
                ratingFilter.maxFilterProperty().addListener(c -> settings.setFilterMaxRating(ratingFilter.getMaxFilter()));
            }

            {//read count
                //load from persistence
                readFilter.setMinFilter(settings.getFilterMinReadCount());
                readFilter.setMaxFilter(settings.getFilterMaxReadCount());

                //add listener
                readFilter.minFilterProperty().addListener(c -> settings.setFilterMinReadCount(readFilter.getMinFilter()));
                readFilter.maxFilterProperty().addListener(c -> settings.setFilterMaxReadCount(readFilter.getMaxFilter()));
            }
        }

        {//sorting
            sortButton.textProperty().bind(settings.sortByProperty());

        }
    }


    private void bindVisibility() {
        //filters
        filterPane.visibleProperty().bind(settings.filtersVisibleProperty());
        filterPane.managedProperty().bind(settings.filtersVisibleProperty());
        //left tree
        leftPanel.visibleProperty().bind(settings.watchesTreeVisibleProperty());
        leftPanel.managedProperty().bind(settings.watchesTreeVisibleProperty());
        //selected galleries indicator box
        rightSelectedBox.visibleProperty().bind(Bindings.isNotEmpty(dao.currentSelectedProperty()));
        //show page switch indicator only if more than one page
        pageIndicator.visibleProperty().bind(
                Bindings.when(galleryModel.maxPagesProperty().greaterThan(1))
                        .then(true).otherwise(false)
        );
    }


    private void populateWatchesTree() {
        watchesTree.setShowRoot(true);
        TreeItem<Pair<String,ApiType>> root = new TreeItem<>(new Pair<>("All", None));

        watchesTree.setRoot(root);
    }



    @FXML
    private void onShowFilters() {
        settings.setFiltersVisible(!settings.getFiltersVisible());
    }

    @FXML
    void setGalleryView(){
        sortButton.setVisible(true);
        settings.setGalleryView(true);
        contentAnchor.getChildren().remove(0);

        galleryIndicator.setStyle("-fx-background-color: green");
        listIndicator.setStyle("-fx-background-color: transparent");
        contentAnchor.getChildren().add(0,galleryView.getView());
    }

    @FXML
    void setListView(){
        sortButton.setVisible(false);
        settings.setGalleryView(false);
        contentAnchor.getChildren().remove(0);

        listIndicator.setStyle("-fx-background-color: green");
        galleryIndicator.setStyle("-fx-background-color: transparent");

        contentAnchor.getChildren().add(0,tableView.getView());
    }

    @FXML
    void onSearch(ActionEvent event) {
        String currentSearch = search.getTextField().getText().toLowerCase();
        l.info("Query database for '{}'",currentSearch);

        settings.setLastSearch(currentSearch);
        search.getEntries().add(currentSearch);

        //todo option
        //search.getTextField().clear();

        galleryModel.invalidatePages();
        galleryPresenter.showPage(0);
    }

    @FXML
    void onShowWatches(){
        settings.setWatchesTreeVisible(!settings.getWatchesTreeVisible());


        Notifications.create().darkStyle().hideAfter(Duration.minutes(1.0)).text("Wait!").owner(rootPane).showWarning();
    }

    public void showErrorMessage(Task task) {
        Window stage = rootPane.getScene().getWindow();


        Scene oldScene = stage.getScene();
        Node oldNode = oldScene.getRoot();

        oldNode.setDisable(true);
        oldNode.setOpacity(0.8);

        StackPane p = new StackPane(oldNode);


        RequestErrorView errorView = new RequestErrorView(p, oldNode, task.getException());
        p.getChildren().add(errorView.getView());

        oldScene.setRoot(p);
    }

    @FXML
    void onSettings(ActionEvent e){
        Stage stage = new Stage();
        stage.setTitle("HWatchFX Settings");

        SettingsView settingsView = new SettingsView();
        stage.setScene(new Scene(settingsView.getView()));
        stage.show();
    }


    @FXML
    void onPreviousPage(){
        l.info("Go to previous page");
        galleryModel.previousPage();
    }

    @FXML
    void onNextPage(){
        l.info("Go to next page");
        galleryModel.nextPage();
    }

    @FXML
    void onFirstPage(){
        l.info("Go to first page");
        galleryModel.goToPage(0);
    }

    @FXML
    void onLastPage(){
        l.info("Go to last page");
        galleryModel.goToPage(galleryModel.getMaxPages() - 1);
    }



    public AnchorPane getContentRoot() {
        return contentAnchor;
    }



    // -------------------------------------
    //          SORTING
    // -------------------------------------
    @FXML
    void onSortDateNewestFirst(){
        l.info("Sort by date newest first");
        settings.setCurrentSorting(DATE_NEWEST_FIRST);
    }

    @FXML
    void onSortDateOldestFirst(){
        l.info("Sort by date oldest first");
        settings.setCurrentSorting(DATE_OLDEST_FIRST);
    }

    @FXML
    void onSortTitleAZ(){
        settings.setCurrentSorting(TITLE_A_Z);
    }

    @FXML
    void onSortTitleZA(){
        settings.setCurrentSorting(TITLE_Z_A);
    }

    @FXML
    void onSortRatingHighestFirst(){
        settings.setCurrentSorting(RATING_HIGHEST_FIRST);
    }

    @FXML
    void onSortRatingLowestFirst(){
        settings.setCurrentSorting(RATING_LOWEST_FIRST);
    }

    @FXML
    void onSortPagesHighestFirst(){
        settings.setCurrentSorting(PAGES_HIGHEST_FIRST);
    }

    @FXML
    void onSortPagesLowestFirst(){
        settings.setCurrentSorting(PAGES_LOWEST_FIRST);
    }
}
