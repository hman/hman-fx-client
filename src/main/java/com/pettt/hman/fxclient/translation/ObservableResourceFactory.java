package com.pettt.hman.fxclient.translation;

import com.pettt.hman.fxclient.views.main.MainPresenter;
import com.pettt.hman.fxclient.views.selectlanguage.SelectlanguagePresenter;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ObservableResourceFactory {

    private Map<String, ObjectProperty<ResourceBundle>> classResources = new HashMap<>();

    public final void putResources(String clazz, ResourceBundle resources) {
        classResources.putIfAbsent(clazz, new SimpleObjectProperty<>());
        resourcesProperty(clazz).set(resources);
    }

    public ObjectProperty<ResourceBundle> resourcesProperty(String clazz) {
        return classResources.getOrDefault(clazz, new SimpleObjectProperty<>());
    }

    public final ResourceBundle getResources(String clazz) {
        return resourcesProperty(clazz).get();
    }

    public StringBinding getClassStringBinding(String clazz, String key) {
        return new StringBinding() {
            { bind(resourcesProperty(clazz)); }
            @Override
            public String computeValue() {
                return getResources(clazz).getString(key);
            }
        };
    }


    public void updateClassResources(){
        String[] presenters = new String[]{
                SelectlanguagePresenter.class.getCanonicalName()
                , MainPresenter.class.getCanonicalName()
        };

        for (String presenter : presenters) {
            String cutName = presenter.substring(0,presenter.length()-9).toLowerCase();
            putResources(presenter, ResourceBundle.getBundle(cutName, Locale.getDefault()));
        }
    }

}