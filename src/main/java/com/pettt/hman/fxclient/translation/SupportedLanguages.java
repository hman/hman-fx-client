package com.pettt.hman.fxclient.translation;

import java.util.Locale;

/**
 * 2017/01/03.
 */
public class SupportedLanguages {
    //native language names
    public static final String ENGLISH = "English";
    public static final String GERMAN = "Deutsch";
    public static final String FRENCH = "Fran\u00e7ais";
    public static final String SPANISH = "Espa\u00f1ol";
    public static final String ITALIAN = "Italiano";
    public static final String POLISH = "Polski";
    public static final String RUSSIAN = "\u0420\u0443\u0441\u0441\u043a\u0438\u0439";
    public static final String JAPANESE = "\u65e5\u672c\u8a9e";
    public static final String CHINESE = "\u4e2d\u6587";
    public static final String KOREAN = "\ud55c\uad6d\uc5b4";

    //locales
    public static final Locale SPAIN = new Locale("es", "ES");
    public static final Locale POLAND = new Locale("pl", "PL");
    public static final Locale RUSSIA = new Locale("ru");

}
